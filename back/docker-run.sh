#!/bin/sh

sleep 12s

pip install -r requirements.txt

./manage.py migrate
./manage.py loaddata project/fixtures/*

./manage.py collectstatic --noinput

celery -A project worker -Q web --beat --loglevel=info \
                                       --pidfile="/tmp/celery.pid" \
                                       --schedule="/tmp/celerybeat-schedule" &

gunicorn project.wsgi:application --log-level=info --bind=back:8000 --reload
