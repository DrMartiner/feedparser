import os

from split_settings.tools import include, optional

ROLE = os.environ.get('ROLE')

# Parts of settings
include(
    'common.py',
    'roles/{}.py'.format(ROLE),  # Current role's settings
    optional('local.py')  # Local settings
)
