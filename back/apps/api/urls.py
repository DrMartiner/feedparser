from rest_framework.routers import DefaultRouter

from . import views

router = DefaultRouter()
router.register(r'source', views.SourceModelViewSet, base_name='source')
router.register(r'news', views.NewsModelViewSet, base_name='news')

urlpatterns = router.urls
