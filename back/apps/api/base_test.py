from rest_framework.test import APITestCase


class BaseApiTest(APITestCase):

    def assertListInResponse(self, serializer_class, qset, response):
        self.assertIn('results', response.data)

        serializer = serializer_class(many=True, instance=qset)
        self.assertEquals(len(serializer.data), len(response.data['results']))
        self.assertListEqual(list(serializer.data), response.data['results'])

    def assertRetrieveInResponse(self, serializer_class, instance, response):
        serializer = serializer_class(instance=instance)
        self.assertDictEqual(serializer.data, response.data)
