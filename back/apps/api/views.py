import logging

from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response

from .filters import NewsFilter
from .serializers import NewsSerializer, SourceSerializer
from apps.news.tasks import run_news_crawler
from apps.news.models import Source, News

logger = logging.getLogger('django.request')


class SourceModelViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Source.objects.all()
    serializer_class = SourceSerializer

    @action(detail=True, methods=['post'])
    def crawler(self, request, pk=None):
        obj = self.get_object()  # type: Source

        try:
            task = self._run_crawler(obj)
            response_status = status.HTTP_202_ACCEPTED

            logger.info(f'Run news crawler TaskID=f{task.id}')
        except Exception as e:
            response_status = status.HTTP_500_INTERNAL_SERVER_ERROR

            logger.error(f'Error occurred at run crawler for SourceID={obj.pk}', exc_info=True)

        return Response({}, status=response_status)

    def _run_crawler(self, source: Source):
        args = (source.source, source.crawler_class)
        return run_news_crawler.apply_async(args=args, queue='web')


class NewsModelViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = News.objects.all()
    serializer_class = NewsSerializer
    filter_class = NewsFilter
