from django_filters.rest_framework import FilterSet

from apps.news.models import News


class NewsFilter(FilterSet):

    class Meta:
        model = News
        fields = ['source']