from rest_framework import serializers

from apps.news.models import Source, News


class SourceSerializer(serializers.ModelSerializer):
    total_count = serializers.SerializerMethodField()

    def get_total_count(self, obj: Source) -> int:
        return obj.total_count

    class Meta:
        model = Source
        fields = '__all__'


class NewsSerializer(serializers.ModelSerializer):

    class Meta:
        model = News
        fields = '__all__'
