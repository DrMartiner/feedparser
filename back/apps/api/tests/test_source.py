from django.contrib.auth import get_user_model
from django_dynamic_fixture import G
from rest_framework import status
from rest_framework.reverse import reverse

from apps.api.base_test import BaseApiTest
from apps.api.serializers import SourceSerializer
from apps.news.models import Source

User = get_user_model()


class SourceApiTest(BaseApiTest):
    URL_LIST = reverse('api_v1:source-list')
    URL_RETRIEVE_NAMED = 'api_v1:source-detail'

    def setUp(self):
        super(SourceApiTest, self).setUp()

        user = G(User)
        self.client.force_login(user)

        self.POST_DATA = {
            'name': 'News name',
            'source': 'http://example.com/rss',
        }

    def test_create(self):
        response = self.client.post(self.URL_LIST, self.POST_DATA)
        self.assertEquals(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        is_created = Source.objects.filter(**self.POST_DATA).exists()
        self.assertFalse(is_created)

    def test_get_detail(self):
        obj = G(Source)  # type: Source

        url = reverse(self.URL_RETRIEVE_NAMED, args=[obj.pk])
        response = self.client.get(url)
        self.assertEquals(response.status_code, status.HTTP_200_OK)

        self.assertRetrieveInResponse(SourceSerializer, obj, response)

    def test_get_list(self):
        for i in range(3):
            G(Source)

        response = self.client.get(self.URL_LIST)
        self.assertEquals(response.status_code, status.HTTP_200_OK)

        self.assertListInResponse(SourceSerializer, Source.objects.all(), response)

    def test_update(self):
        obj = G(Source)  # type: Source

        url = reverse(self.URL_RETRIEVE_NAMED, args=[obj.pk])
        response = self.client.post(url, self.POST_DATA)
        self.assertEquals(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        is_updated = Source.objects.filter(**self.POST_DATA).exists()
        self.assertFalse(is_updated)

    def test_delete(self):
        obj = G(Source)  # type: Source

        url = reverse(self.URL_RETRIEVE_NAMED, args=[obj.pk])
        response = self.client.delete(url)
        self.assertEquals(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        is_exists = Source.objects.filter(pk=obj.pk).exists()
        self.assertTrue(is_exists)
