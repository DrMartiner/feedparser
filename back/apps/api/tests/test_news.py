from django.contrib.auth import get_user_model
from django_dynamic_fixture import G
from rest_framework import status
from rest_framework.reverse import reverse

from apps.api.base_test import BaseApiTest
from apps.api.serializers import NewsSerializer
from apps.news.models import Source, News

User = get_user_model()


class NewsApiTest(BaseApiTest):
    URL_LIST = reverse('api_v1:news-list')
    URL_RETRIEVE_NAMED = 'api_v1:news-detail'

    def setUp(self):
        super(NewsApiTest, self).setUp()

        user = G(User)
        self.client.force_login(user)

        self.source = G(Source)
        self.POST_DATA = {
            'source': self.source.pk,
            'title': 'News title',
            'link': 'http://example.com/news/123',

        }

    def test_create(self):
        response = self.client.post(self.URL_LIST, self.POST_DATA)
        self.assertEquals(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        is_created = News.objects.filter(**self.POST_DATA).exists()
        self.assertFalse(is_created)

    def test_get_detail(self):
        obj = G(News)  # type: News

        url = reverse(self.URL_RETRIEVE_NAMED, args=[obj.pk])
        response = self.client.get(url)
        self.assertEquals(response.status_code, status.HTTP_200_OK)

        self.assertRetrieveInResponse(NewsSerializer, obj, response)

    def test_get_list(self):
        for i in range(3):
            G(News)

        response = self.client.get(self.URL_LIST)
        self.assertEquals(response.status_code, status.HTTP_200_OK)

        self.assertListInResponse(NewsSerializer, News.objects.all(), response)

    def test_update(self):
        obj = G(News)  # type: News

        url = reverse(self.URL_RETRIEVE_NAMED, args=[obj.pk])
        response = self.client.post(url, self.POST_DATA)
        self.assertEquals(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        is_updated = News.objects.filter(**self.POST_DATA).exists()
        self.assertFalse(is_updated)

    def test_delete(self):
        obj = G(News)  # type: News

        url = reverse(self.URL_RETRIEVE_NAMED, args=[obj.pk])
        response = self.client.delete(url)
        self.assertEquals(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        is_exists = News.objects.filter(pk=obj.pk).exists()
        self.assertTrue(is_exists)

    def test_filter_by_source(self):
        source = G(Source)  # type: Source
        for i in range(3):
            G(News, source=source)
        G(News)

        response = self.client.get(self.URL_LIST, data={'source': source.pk})
        self.assertEquals(response.status_code, status.HTTP_200_OK)

        self.assertListInResponse(NewsSerializer, News.objects.filter(source=source), response)
