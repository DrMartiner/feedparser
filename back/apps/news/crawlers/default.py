import logging

import feedparser

logger = logging.getLogger('django.request')


class DefaultRssCrawler:
    def __init__(self, source: str):
        self.source = source

    def load(self) -> list:
        try:
            feed = feedparser.parse(self.source)
        except Exception as e:
            logger.error(f'Error occurred at loading feed "{self.source}"', exc_info=True)
            return []

        news_list = []
        for news in feed.entries:
            try:
                news_list.append({
                    'title': news.title,
                    'link': news.link,
                })
            except Exception as e:
                logger.error(f'Error occurred at parse a news from feed "{self.source}"', exc_info=True)

        return news_list
