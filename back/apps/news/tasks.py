import logging

from celery import shared_task
from django.db import IntegrityError

from apps.news.models import Source, News
from .news_loader import NewsLoader

logger = logging.getLogger('django.request')


@shared_task
def run_news_crawler(source_link: str, crawler_class: str):
    loader = NewsLoader(source_link, crawler_class)
    news_list = loader.load_news()

    try:
        source = Source.objects.get(source=source_link)
    except Source.DoesNotExist:
        return logger.error(f'Source.source={source_link} doesn\'t exists')

    for news_data in news_list:
        try:
            News.objects.get_or_create(source=source, **news_data)
        except IntegrityError:
            pass
        except Exception as e:
            logger.error(f'Error occurred at create News for SourceID={source.id}', exc_info=True)
