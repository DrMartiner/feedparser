from django.contrib import admin
from django.utils.translation import gettext_lazy as _

from .models import Source, News


class NewsInlineAdmin(admin.StackedInline):
    extra = 0
    model = News

    fieldsets = (
        (_('Main'), {'fields': ('title', 'link')}),
        (_('Dates'), {'fields': ('created', 'updated')}),
    )
    readonly_fields = ['created', 'updated']

    def get_queryset(self, request):
        return super(NewsInlineAdmin, self).get_queryset(request)[:5]

    def has_add_permission(self, request, obj):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Source)
class SourceAdmin(admin.ModelAdmin):
    list_display = ['name', 'source', 'total_count']
    fieldsets = (
        (_('Main'), {'fields': ('name', 'source', 'total_count')}),
        (_('Service'), {'fields': ('crawler_class', )}),
    )
    readonly_fields = ['total_count']


@admin.register(News)
class NewsAdmin(admin.ModelAdmin):
    list_display = ['short_title', 'source', 'created', 'updated']
    list_filter = ['source', 'created', 'updated']
    readonly_fields = ['created', 'updated']

    fieldsets = (
        (_('Main'), {'fields': ('source', 'title', 'link')}),
        (_('Dates'), {'fields': ('created', 'updated')}),
    )

    def short_title(self, obj:News) -> str:
        return obj.title

    short_title.admin_order_field = 'title'
