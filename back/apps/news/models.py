from django.db import models
from django.template.defaultfilters import truncatechars


class Source(models.Model):
    name = models.CharField(max_length=32)
    source = models.URLField(max_length=512, unique=True)

    crawler_class = models.CharField(max_length=128, default='default.DefaultRssCrawler')

    def __str__(self):
        return self.name

    @property
    def total_count(self) -> int:
        return self.news_set.count()

    class Meta:
        app_label = 'news'
        verbose_name = 'source'
        verbose_name_plural = 'Sources'


class News(models.Model):
    source = models.ForeignKey('Source', on_delete=models.CASCADE)
    title = models.CharField(max_length=512)
    link = models.URLField(unique=True, max_length=512)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return truncatechars(self.title, 32)

    class Meta:
        app_label = 'news'
        ordering = ['created']
        verbose_name = 'news'
        verbose_name_plural = 'News'
