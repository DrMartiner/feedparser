import logging
from importlib import import_module

logger = logging.getLogger('django.request')


class NewsLoader:
    _source = ''

    _class_prefix = 'apps.news.crawlers'
    _crawler_class_path = None  # type: str
    _classes = {}

    def __init__(self, source: str, crawler_class: str) -> None:
        self._source = source
        self._crawler_class_path = f'{self._class_prefix}.{crawler_class}'

    def load_news(self) -> list:
        try:
            crawler_class = self._import_crawler_class(self._crawler_class_path)
        except Exception as e:
            pass  # TODO: to log it
            logger.error(f'Error occurred at importing crawler class {crawler_class}', exc_info=True)
            return []

        crawler = crawler_class(self._source)
        return crawler.load()

    def _import_crawler_class(self, path: str) -> object:
        crawler_class = NewsLoader._classes.get(path)
        if crawler_class:
            return crawler_class

        module_path = '.'.join(path.split('.')[:-1])
        module = import_module(module_path)
        crawler_class = getattr(module, path.split('.')[-1])

        NewsLoader._classes[path] = crawler_class

        return crawler_class
