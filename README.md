# Table of Contents
- [To run demo](#to-run-demo)
- [To develop](#to-develop)
- [Diagrams](#diagrams)

# To run demo

- run this commands:
```bash
git clone https://bitbucket.org/drmartiner/test_feedparser.git
cd test_feedparser
docker-compose up -d nginx
```

- open in a browser [localhost:8080](http://localhost:8080/ "Local demo link") - press "**crawl**" and "**refresh**"


# To develop

## Back-end
- create configure file: ```back/.env.ini```
- define there:
```
DEBUG=on

BROKER_URL=redis://locahost:6379
CELERY_BROKER_URL=redis://locahost:6379
CELERY_RESULT_BACKEND_URL=redis://locahost:6379

POSTGRES_HOST=localhost
POSTGRES_DB=test
POSTGRES_USER=test
POSTGRES_PASSWORD=password
```

- run Redis and Postgres at your localhost 
- create database *test*, user *test*  with password *password*
- install virtualenv package
- run this commands:

```bash
cd back
virtualenv --prompt="(.test) "  venv
source venv/bin/activate

export ROLE=debug

./manage.py migrate
./manage.py loaddata project/fixtures/*

./manage.py runserver
```
- open [localhost:8000](http://localhost:8000/ "Local develop")

### To run tests
```bash
export ROLE=test
./manage.py test
```


## Front-end
- run this commands:
```bash
cd front
ng serve --proxy-config proxy.conf.json
```
- open [localhost:4200](http://localhost:4200/ "Local develop")
- at the end you should build your app via command: ```ng build```

# Task
A Python script should be created that does informational analysis of English news feeds.

Result of the script should be an html-report that contains following information:

1.  Top-5 mentioned news in the news feeds with the links to the primary source piece of news;
2.  List of unique news for every news feed with links to news
3.  Quantity of news in every news feed.

As a source following news should be used:

- [Huffingtonpost](https://www.huffingtonpost.com/section/world-news/feed "")
- [Reuters](http://feeds.reuters.com/Reuters/worldNews "")
- [Nytimes](http://rss.nytimes.com/services/xml/rss/nyt/World.xml "")
- [CNN](http://rss.cnn.com/rss/edition_world.rss "")
- [BBC](http://feeds.bbci.co.uk/news/world/rss.xml "")
- [FOX News](http://feeds.foxnews.com/foxnews/world "")
- [USA Today]( "http://rssfeeds.usatoday.com/UsatodaycomWorld-TopStories")
- [The Guardian]( "https://www.theguardian.com/world/rss")
- [Washingtonpost]( "http://feeds.washingtonpost.com/rss/world")

UI Design Considerations and general constraints
The application should be done as a script file that can be ran via console. It is allowed to create own library or to use existing ones. 
Configuring of the list of news feeds should be done via configuration file. Configuration file should be located in the same directory as the script file. It is ok for the script to write information messages and error messages to the output. The result of the script work should be html-report file located in the same directory as the script itself. 


# Diagrams

### Use case diagram
![Use case diagram](docs/img/use_case.png)

### Deploy diagram
![Deploy diagram](docs/img/deployment.png)


### Sequence diagram
![Sequence diagram](docs/img/crawl_news.png)