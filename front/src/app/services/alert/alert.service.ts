import { Injectable } from '@angular/core';
import { Observable, Subject } from "rxjs/index";
import { Alert, AlertType } from './alert.model';

@Injectable()
export class AlertService {
    private subject = new Subject<Alert>();

    public getAlert(): Observable<any> {
        return this.subject.asObservable();
    }

    public success(message: any): void {
        this.alert(AlertType.Success, message);
    }

    public error(message: any): void {
        this.alert(AlertType.Error, message);
    }

    public info(message: any): void {
        this.alert(AlertType.Info, message);
    }

    public warn(message: any): void {
        this.alert(AlertType.Warning, message);
    }

    public alert(type: AlertType, message: any): void {
        this.subject.next(<Alert>{ type: type, message: message });
    }

    public clear(): void {
        this.subject.next();
    }
}
