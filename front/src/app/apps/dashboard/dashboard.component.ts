import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { AlertService } from "../../services";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public sources: Array<Object> = [];

  constructor(
    private http: HttpClient,
    private alertService: AlertService,
  ) { }

  ngOnInit() {
    this.loadFeeds();
  }

  public loadFeeds(): void {
    this.sources = [];
    this.http.get('/api/v1/source/')
      .subscribe((response) => {
        this.sources = response['results'] || [];
      }, (error) => {
        this.alertService.error('Error occurred at sources list loading.');
      });
  }

  public runCrawler(sourceId: Number): void {
    const url = `/api/v1/source/${sourceId}/crawler/`;
    this.http.post(url, {})
      .subscribe((response) => {
        this.alertService.success('Crawler was run.');
      }, (error) => {
        this.alertService.error('Error occurred at running crawler.');
      });
  }
}
