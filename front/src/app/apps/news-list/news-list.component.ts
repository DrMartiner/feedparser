import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { AlertService } from "../../services";
import { HttpClient, HttpParams } from '@angular/common/http';

@Component({
  selector: 'app-news-list',
  templateUrl: './news-list.component.html',
  styleUrls: ['./news-list.component.scss']
})
export class NewsListComponent implements OnInit {
  @Input() public title: String = null;

  @Input() private paginateBy = 20;
  @Input() private showPagination: boolean = true;

  private sourceId: Number = null;
  public sourceDetail: Object = null;

  public newsList: Array<Object> = [];
  public currentPage = 1;
  public collectionSize = null;

  constructor(
    private http: HttpClient,
    private router: Router,
    private route: ActivatedRoute,
    private alertService: AlertService,
  ) { }

  ngOnInit() {
    this.route.params.subscribe(routeParams => {
      this.sourceId = routeParams['id'];
      const url = `/api/v1/source/${this.sourceId}/`;
      this.http.get(url)
        .subscribe((response) => {
          this.sourceDetail = response;

          this.loadNewsList();
        }, (error) => {
          this.alertService.error('Error occurred at source detail loading.');
        });
      });
  }

  private loadNewsList(): void {
      let params = {
        source: String(this.sourceId),
        limit: String(this.paginateBy),
        offset: String(this.paginateBy * (this.currentPage - 1))
      };

      this.http.get('/api/v1/news/', {params})
        .subscribe((response) => {
          this.newsList = response['results'] || [];
          this.collectionSize = response['count'];
        }, (error) => {
          this.alertService.error('Error occurred at all news list loading.');
        });
  }

  public onPageChange(page): void {
    if (!page)
      return ;

    this.currentPage = page;
    this.loadNewsList();
  }
}
