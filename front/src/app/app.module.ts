import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from "@angular/common/http";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { AlertService } from "./services";
import { DashboardComponent } from './apps/dashboard/dashboard.component';
import { AllNewsComponent } from './apps/all-news/all-news.component';
import { TopNewsComponent } from './apps/top-news/top-news.component';
import { NewsListComponent } from './apps/news-list/news-list.component';

const appRoutes: Routes =[
    { path: '', component: DashboardComponent},
    { path: 'feed/:id/news/top-5', component: TopNewsComponent, pathMatch: 'full'},
    { path: 'feed/:id/news/all', component: AllNewsComponent, pathMatch: 'full'},
    // { path: '**', component: NotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    AllNewsComponent,
    TopNewsComponent,
    NewsListComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    NgbModule.forRoot(),
  ],
  providers: [AlertService],
  bootstrap: [AppComponent]
})
export class AppModule { }
