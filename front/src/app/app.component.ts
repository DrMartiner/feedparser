import { Component } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Alert, AlertService, AlertType } from "./services";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public alerts: Alert[] = [];

  constructor(
    private http: HttpClient,
    private alertService: AlertService,
  ) { }

  ngOnInit() {
    this.alertService.getAlert().subscribe((alert: Alert) => {
      if (!alert) {
        this.alerts = [];
      }
      else {
        this.alerts.push(alert);
        setTimeout(() => this.removeAlert(alert), 3000);
      }
    });
  }

  public removeAlert(alert: Alert) {
    this.alerts = this.alerts.filter(x => x !== alert);
  }

  public getAlertType(alert: Alert) {
    if (!alert) {
      return AlertType.Success;
    }

    switch (alert.type) {
      case AlertType.Success:
        return 'success';
      case AlertType.Error:
        return 'danger';
      case AlertType.Info:
        return 'info';
      case AlertType.Warning:
        return 'warning';
    }
    return '';
  }
}
