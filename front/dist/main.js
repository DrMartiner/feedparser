(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"outer-alerts\">\n  <ngb-alert *ngFor=\"let alert of alerts\" [type]=\"getAlertType(alert)\" (close)=\"removeAlert(alert)\">{{ alert.message }}</ngb-alert>\n</div>\n\n<div class=\"container\">\n  <router-outlet></router-outlet>\n</div>\n"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container {\n  padding-top: 20px; }\n\n.outer-alerts {\n  position: fixed;\n  top: 60px;\n  left: 20px;\n  right: 20px;\n  z-index: 9999;\n  max-width: 60%; }\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./services */ "./src/app/services/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AppComponent = /** @class */ (function () {
    function AppComponent(http, alertService) {
        this.http = http;
        this.alertService = alertService;
        this.alerts = [];
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.alertService.getAlert().subscribe(function (alert) {
            if (!alert) {
                _this.alerts = [];
            }
            else {
                _this.alerts.push(alert);
                setTimeout(function () { return _this.removeAlert(alert); }, 3000);
            }
        });
    };
    AppComponent.prototype.removeAlert = function (alert) {
        this.alerts = this.alerts.filter(function (x) { return x !== alert; });
    };
    AppComponent.prototype.getAlertType = function (alert) {
        if (!alert) {
            return _services__WEBPACK_IMPORTED_MODULE_2__["AlertType"].Success;
        }
        switch (alert.type) {
            case _services__WEBPACK_IMPORTED_MODULE_2__["AlertType"].Success:
                return 'success';
            case _services__WEBPACK_IMPORTED_MODULE_2__["AlertType"].Error:
                return 'danger';
            case _services__WEBPACK_IMPORTED_MODULE_2__["AlertType"].Info:
                return 'info';
            case _services__WEBPACK_IMPORTED_MODULE_2__["AlertType"].Warning:
                return 'warning';
        }
        return '';
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _services__WEBPACK_IMPORTED_MODULE_2__["AlertService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./services */ "./src/app/services/index.ts");
/* harmony import */ var _apps_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./apps/dashboard/dashboard.component */ "./src/app/apps/dashboard/dashboard.component.ts");
/* harmony import */ var _apps_all_news_all_news_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./apps/all-news/all-news.component */ "./src/app/apps/all-news/all-news.component.ts");
/* harmony import */ var _apps_top_news_top_news_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./apps/top-news/top-news.component */ "./src/app/apps/top-news/top-news.component.ts");
/* harmony import */ var _apps_news_list_news_list_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./apps/news-list/news-list.component */ "./src/app/apps/news-list/news-list.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











var appRoutes = [
    { path: '', component: _apps_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_7__["DashboardComponent"] },
    { path: 'feed/:id/news/top-5', component: _apps_top_news_top_news_component__WEBPACK_IMPORTED_MODULE_9__["TopNewsComponent"], pathMatch: 'full' },
    { path: 'feed/:id/news/all', component: _apps_all_news_all_news_component__WEBPACK_IMPORTED_MODULE_8__["AllNewsComponent"], pathMatch: 'full' },
];
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"],
                _apps_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_7__["DashboardComponent"],
                _apps_all_news_all_news_component__WEBPACK_IMPORTED_MODULE_8__["AllNewsComponent"],
                _apps_top_news_top_news_component__WEBPACK_IMPORTED_MODULE_9__["TopNewsComponent"],
                _apps_news_list_news_list_component__WEBPACK_IMPORTED_MODULE_10__["NewsListComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forRoot(appRoutes),
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbModule"].forRoot(),
            ],
            providers: [_services__WEBPACK_IMPORTED_MODULE_6__["AlertService"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/apps/all-news/all-news.component.html":
/*!*******************************************************!*\
  !*** ./src/app/apps/all-news/all-news.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-news-list [title]=\"'All news'\" [paginateBy]=\"20\"></app-news-list>\n"

/***/ }),

/***/ "./src/app/apps/all-news/all-news.component.scss":
/*!*******************************************************!*\
  !*** ./src/app/apps/all-news/all-news.component.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/apps/all-news/all-news.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/apps/all-news/all-news.component.ts ***!
  \*****************************************************/
/*! exports provided: AllNewsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AllNewsComponent", function() { return AllNewsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AllNewsComponent = /** @class */ (function () {
    function AllNewsComponent() {
    }
    AllNewsComponent.prototype.ngOnInit = function () { };
    AllNewsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-all-news',
            template: __webpack_require__(/*! ./all-news.component.html */ "./src/app/apps/all-news/all-news.component.html"),
            styles: [__webpack_require__(/*! ./all-news.component.scss */ "./src/app/apps/all-news/all-news.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], AllNewsComponent);
    return AllNewsComponent;
}());



/***/ }),

/***/ "./src/app/apps/dashboard/dashboard.component.html":
/*!*********************************************************!*\
  !*** ./src/app/apps/dashboard/dashboard.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n  <div class=\"col-lg-12 col-md-12 col-xs-12 col-sm-12\">\n    <nav aria-label=\"breadcrumb\">\n      <ol class=\"breadcrumb\">\n        <li class=\"breadcrumb-item active\" aria-current=\"page\">Dashboard</li>\n      </ol>\n    </nav>\n\n    <table class=\"table table-bordered table-sm table-responsive-lg table-responsive-md table-responsive-sm\" *ngIf=\"sources.length\">\n      <thead>\n      <tr>\n        <td>Name</td>\n        <td>Total news count</td>\n        <td>News lists</td>\n        <td>Actions</td>\n      </tr>\n      </thead>\n      <tbody>\n      <tr *ngFor=\"let source of sources\">\n        <td>\n          <a [href]=\"source.source\" target=\"_blank\">\n            {{ source.name }}\n          </a>\n        </td>\n        <td>\n          {{ source.total_count }}\n        </td>\n        <td>\n          <a [routerLink]=\"['/feed', source.id, 'news', 'top-5']\">\n            Top-5\n          </a> |\n          <a [routerLink]=\"['/feed', source.id, 'news', 'all']\">\n            All news\n          </a>\n        </td>\n        <td>\n          <button class=\"btn btn-sm btn-success\" (click)=\"runCrawler(source.id)\">Crawl</button>\n        </td>\n      </tr>\n      </tbody>\n      <tfoot>\n        <tr>\n          <td colspan=\"4\" class=\"text-center\">\n            <button class=\"btn btn-danger btn-lg\" (click)=\"loadFeeds()\">Refresh feeds</button>\n          </td>\n        </tr>\n      </tfoot>\n    </table>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/apps/dashboard/dashboard.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/apps/dashboard/dashboard.component.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/apps/dashboard/dashboard.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/apps/dashboard/dashboard.component.ts ***!
  \*******************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services */ "./src/app/services/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DashboardComponent = /** @class */ (function () {
    function DashboardComponent(http, alertService) {
        this.http = http;
        this.alertService = alertService;
        this.sources = [];
    }
    DashboardComponent.prototype.ngOnInit = function () {
        this.loadFeeds();
    };
    DashboardComponent.prototype.loadFeeds = function () {
        var _this = this;
        this.sources = [];
        this.http.get('/api/v1/source/')
            .subscribe(function (response) {
            _this.sources = response['results'] || [];
        }, function (error) {
            _this.alertService.error('Error occurred at sources list loading.');
        });
    };
    DashboardComponent.prototype.runCrawler = function (sourceId) {
        var _this = this;
        var url = "/api/v1/source/" + sourceId + "/crawler/";
        this.http.post(url, {})
            .subscribe(function (response) {
            _this.alertService.success('Crawler was run.');
        }, function (error) {
            _this.alertService.error('Error occurred at running crawler.');
        });
    };
    DashboardComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-dashboard',
            template: __webpack_require__(/*! ./dashboard.component.html */ "./src/app/apps/dashboard/dashboard.component.html"),
            styles: [__webpack_require__(/*! ./dashboard.component.scss */ "./src/app/apps/dashboard/dashboard.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _services__WEBPACK_IMPORTED_MODULE_2__["AlertService"]])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "./src/app/apps/news-list/news-list.component.html":
/*!*********************************************************!*\
  !*** ./src/app/apps/news-list/news-list.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n  <div class=\"col-lg-12 col-md-12 col-xs-12 col-sm-12\">\n    <nav aria-label=\"breadcrumb\">\n      <ol class=\"breadcrumb\">\n        <li class=\"breadcrumb-item\">\n          <a [routerLink]=\"['/']\">Dashboard</a>\n        </li>\n        <li class=\"breadcrumb-item active\" aria-current=\"page\" *ngIf=\"title\">\n          <span *ngIf=\"sourceDetail\">Feed \"{{ sourceDetail.name }}\"</span> - {{ title }}\n        </li>\n      </ol>\n    </nav>\n\n    <table class=\"table table-bordered table-sm table-responsive-lg table-responsive-md table-responsive-sm\" *ngIf=\"newsList.length\">\n      <thead>\n      <tr>\n        <td>News</td>\n      </tr>\n      </thead>\n      <tbody>\n      <tr *ngFor=\"let news of newsList\">\n        <td>\n          <a [href]=\"news.link\" target=\"_blank\">\n            {{ news.title }}\n          </a>\n        </td>\n      </tr>\n      </tbody>\n    </table>\n\n    <div *ngIf=\"showPagination && collectionSize\">\n      <ngb-pagination\n        [(collectionSize)]=\"collectionSize\"\n        [(pageSize)]=\"paginateBy\"\n        [(page)]=\"currentPage\"\n        [rotate]=\"true\" [boundaryLinks]=\"true\"\n        (pageChange)=\"onPageChange($event)\"></ngb-pagination>\n\n      <!--ngb-pagination [collectionSize]=\"120\" [(page)]=\"page\" [maxSize]=\"5\" (pageChange)=\"onPageChange($event)\"></ngb-pagination-->\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/apps/news-list/news-list.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/apps/news-list/news-list.component.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/apps/news-list/news-list.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/apps/news-list/news-list.component.ts ***!
  \*******************************************************/
/*! exports provided: NewsListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewsListComponent", function() { return NewsListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services */ "./src/app/services/index.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var NewsListComponent = /** @class */ (function () {
    function NewsListComponent(http, router, route, alertService) {
        this.http = http;
        this.router = router;
        this.route = route;
        this.alertService = alertService;
        this.title = null;
        this.paginateBy = 20;
        this.showPagination = true;
        this.sourceId = null;
        this.sourceDetail = null;
        this.newsList = [];
        this.currentPage = 1;
        this.collectionSize = null;
    }
    NewsListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (routeParams) {
            _this.sourceId = routeParams['id'];
            var url = "/api/v1/source/" + _this.sourceId + "/";
            _this.http.get(url)
                .subscribe(function (response) {
                _this.sourceDetail = response;
                _this.loadNewsList();
            }, function (error) {
                _this.alertService.error('Error occurred at source detail loading.');
            });
        });
    };
    NewsListComponent.prototype.loadNewsList = function () {
        var _this = this;
        var params = {
            source: String(this.sourceId),
            limit: String(this.paginateBy),
            offset: String(this.paginateBy * (this.currentPage - 1))
        };
        this.http.get('/api/v1/news/', { params: params })
            .subscribe(function (response) {
            _this.newsList = response['results'] || [];
            _this.collectionSize = response['count'];
        }, function (error) {
            _this.alertService.error('Error occurred at all news list loading.');
        });
    };
    NewsListComponent.prototype.onPageChange = function (page) {
        if (!page)
            return;
        this.currentPage = page;
        this.loadNewsList();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], NewsListComponent.prototype, "title", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], NewsListComponent.prototype, "paginateBy", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Boolean)
    ], NewsListComponent.prototype, "showPagination", void 0);
    NewsListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-news-list',
            template: __webpack_require__(/*! ./news-list.component.html */ "./src/app/apps/news-list/news-list.component.html"),
            styles: [__webpack_require__(/*! ./news-list.component.scss */ "./src/app/apps/news-list/news-list.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _services__WEBPACK_IMPORTED_MODULE_2__["AlertService"]])
    ], NewsListComponent);
    return NewsListComponent;
}());



/***/ }),

/***/ "./src/app/apps/top-news/top-news.component.html":
/*!*******************************************************!*\
  !*** ./src/app/apps/top-news/top-news.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-news-list [title]=\"'Top-5'\" [paginateBy]=\"5\" [showPagination]=\"false\"></app-news-list>\n"

/***/ }),

/***/ "./src/app/apps/top-news/top-news.component.scss":
/*!*******************************************************!*\
  !*** ./src/app/apps/top-news/top-news.component.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/apps/top-news/top-news.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/apps/top-news/top-news.component.ts ***!
  \*****************************************************/
/*! exports provided: TopNewsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TopNewsComponent", function() { return TopNewsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TopNewsComponent = /** @class */ (function () {
    function TopNewsComponent() {
    }
    TopNewsComponent.prototype.ngOnInit = function () { };
    TopNewsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-top-news',
            template: __webpack_require__(/*! ./top-news.component.html */ "./src/app/apps/top-news/top-news.component.html"),
            styles: [__webpack_require__(/*! ./top-news.component.scss */ "./src/app/apps/top-news/top-news.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], TopNewsComponent);
    return TopNewsComponent;
}());



/***/ }),

/***/ "./src/app/services/alert/alert.model.ts":
/*!***********************************************!*\
  !*** ./src/app/services/alert/alert.model.ts ***!
  \***********************************************/
/*! exports provided: Alert, AlertType */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Alert", function() { return Alert; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlertType", function() { return AlertType; });
var Alert = /** @class */ (function () {
    function Alert() {
    }
    return Alert;
}());

var AlertType;
(function (AlertType) {
    AlertType[AlertType["Success"] = 0] = "Success";
    AlertType[AlertType["Error"] = 1] = "Error";
    AlertType[AlertType["Info"] = 2] = "Info";
    AlertType[AlertType["Warning"] = 3] = "Warning";
})(AlertType || (AlertType = {}));


/***/ }),

/***/ "./src/app/services/alert/alert.service.ts":
/*!*************************************************!*\
  !*** ./src/app/services/alert/alert.service.ts ***!
  \*************************************************/
/*! exports provided: AlertService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlertService", function() { return AlertService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_index__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/index */ "./node_modules/rxjs/index.js");
/* harmony import */ var rxjs_index__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(rxjs_index__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _alert_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./alert.model */ "./src/app/services/alert/alert.model.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AlertService = /** @class */ (function () {
    function AlertService() {
        this.subject = new rxjs_index__WEBPACK_IMPORTED_MODULE_1__["Subject"]();
    }
    AlertService.prototype.getAlert = function () {
        return this.subject.asObservable();
    };
    AlertService.prototype.success = function (message) {
        this.alert(_alert_model__WEBPACK_IMPORTED_MODULE_2__["AlertType"].Success, message);
    };
    AlertService.prototype.error = function (message) {
        this.alert(_alert_model__WEBPACK_IMPORTED_MODULE_2__["AlertType"].Error, message);
    };
    AlertService.prototype.info = function (message) {
        this.alert(_alert_model__WEBPACK_IMPORTED_MODULE_2__["AlertType"].Info, message);
    };
    AlertService.prototype.warn = function (message) {
        this.alert(_alert_model__WEBPACK_IMPORTED_MODULE_2__["AlertType"].Warning, message);
    };
    AlertService.prototype.alert = function (type, message) {
        this.subject.next({ type: type, message: message });
    };
    AlertService.prototype.clear = function () {
        this.subject.next();
    };
    AlertService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])()
    ], AlertService);
    return AlertService;
}());



/***/ }),

/***/ "./src/app/services/index.ts":
/*!***********************************!*\
  !*** ./src/app/services/index.ts ***!
  \***********************************/
/*! exports provided: Alert, AlertType, AlertService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _alert_alert_model__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./alert/alert.model */ "./src/app/services/alert/alert.model.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Alert", function() { return _alert_alert_model__WEBPACK_IMPORTED_MODULE_0__["Alert"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AlertType", function() { return _alert_alert_model__WEBPACK_IMPORTED_MODULE_0__["AlertType"]; });

/* harmony import */ var _alert_alert_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./alert/alert.service */ "./src/app/services/alert/alert.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AlertService", function() { return _alert_alert_service__WEBPACK_IMPORTED_MODULE_1__["AlertService"]; });





/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/alex/projects/test_tasks/akvelon/front/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map